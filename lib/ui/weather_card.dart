import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/model/hour_weather_dao.dart';
import 'package:flutter_app/pages/weather_detail_page.dart';
import 'package:flutter_app/styles.dart';

import '../utils.dart';

class WeatherCard extends StatelessWidget{

  WeatherCard(this.hourWeather);

  final WeatherData hourWeather;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(12.0, 4.0, 12.0, 4.0),
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => WeatherDetailPage(data: hourWeather)),
          );
        },
        child: Padding(
          padding: const EdgeInsets.fromLTRB(12.0, 8.0, 12.0, 8.0),
          child: Row(
              children: <Widget>[
                Text(Utils.formatTemperature(hourWeather.main.temp), style: TextStyle(
                  fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue
                ),
                ),
                SizedBox(width: 8,),
                Expanded(
                   child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                  child: Text(hourWeather.weathersFirst() != null ? hourWeather.weathersFirst().description : '',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.lightBlueAccent)
                                    ,)
                              ),
                              Expanded(
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(Utils.formatDateTime(hourWeather.dt),
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.blueGrey,
                                    ),
                                    textAlign: TextAlign.right,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text('Pressure: ${hourWeather.main.pressure.toString()}',
                                style: simpleTextStyle,)
                            ],
                          ),
                          Row(
                            children: [
                              Text('Humidity: ${hourWeather.main.humidity.toString()}',
                                style: simpleTextStyle,),
                            ],
                          )
                        ]
                    )
                )

              ]
          ),
        ),
      ),
    );
  }
}

//),
//SizedBox(height: 5.0),
//Text(dayWeather.main.temp.toString(),
//style: TextStyle(
//fontSize: 18,
//color: Colors.grey
//),
//)
