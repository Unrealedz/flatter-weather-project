class DayWeather{
  DateTime date;
  int temperature;

  DayWeather({
    this.date,
    this.temperature});
}