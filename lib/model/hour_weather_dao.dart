import 'dart:convert';

class FiveDaysWeatherResponse{
  List<WeatherData> listDao;

  FiveDaysWeatherResponse({this.listDao});

  factory FiveDaysWeatherResponse.fromJson(String body) {
    var list = jsonDecode(body)['list'] as List;
    List<WeatherData> daoList = list.map<WeatherData>((i) => WeatherData.fromJson(i)).toList();
    return FiveDaysWeatherResponse(
        listDao: daoList
    );
  }
}

class WeatherData{
  DateTime dt;
  Main main;
  List<Weather> weathers;

  WeatherData({this.dt, this.main, this.weathers});

  factory WeatherData.fromJson(Map<String, dynamic> json) {
    var time = DateTime.fromMillisecondsSinceEpoch(json['dt']*1000);
    var list = json['weather'] as List;
    List<Weather> weatherList = list.map<Weather>((i) => Weather.fromJson(i)).toList();

    return WeatherData(
        dt: time,
        main: Main.fromJson(json['main']),
        weathers: weatherList
    );
  }

  Weather weathersFirst() => weathers != null && weathers.length > 0 ? weathers[0] : null;
}

class Main{
  double temp;
  int pressure;
  int humidity;

  Main({this.temp, this.pressure, this.humidity});

  factory Main.fromJson(Map<String, dynamic> json) {
    return Main(
      //TODO sometime a server return this value as int. To avoid exception need to cast a parsed value to double
        temp: (json['temp'] as num).toDouble(),
        pressure: json['pressure'],
        humidity: json['humidity']
    );
  }
}

class Weather{
  String main;
  String description;

  Weather({this.main, this.description});
  factory Weather.fromJson(Map<String, dynamic> json) {
    return Weather(
        main: json['main'],
        description: json['description']
    );
  }
  
}