import 'dart:ui';

import 'package:flutter/material.dart';

final simpleTextStyle = TextStyle(
    fontSize: 14,
    color: Colors.blueGrey);