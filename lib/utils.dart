import 'package:intl/intl.dart';

class Utils{
  static String formatDateTime(DateTime dateTime){
    return DateFormat('yyyy-MM-dd – kk:mm').format(dateTime);
  }

  static String formatShortDateTime(DateTime dateTime){
    return DateFormat.MMMMd('en_US').add_Hm().format(dateTime);
  }

  static String formatTemperature(double temp){
    return '${temp.ceil()} °C';
  }
}
