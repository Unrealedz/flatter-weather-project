class BaseApiResponse<T>{
  T data;
  String errorMessage;
  int errorCode;

  BaseApiResponse({this.data, this.errorMessage, this.errorCode});
}