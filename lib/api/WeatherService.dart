import 'package:flutter_app/model/hour_weather_dao.dart';
import 'package:http/http.dart' as http;

import 'model/base_response.dart';

class WeatherService {
  static const API = 'http://api.openweathermap.org/data/2.5';

  static const headers = {'Content-Type': 'application/json'};
  static const apiKey = '8b4e1e21afc5cdf82a96bc1d0d9e327b';
  static const cityName = 'Lviv';

  Future<BaseApiResponse<FiveDaysWeatherResponse>> getFiveDaysWeather() {
    return http.get(API + '/forecast?q=$cityName&units=metric&appid=$apiKey', headers: headers).then((data) {
      if (data.statusCode == 200) {
        final parsedResponse = FiveDaysWeatherResponse.fromJson(data.body);
         return BaseApiResponse<FiveDaysWeatherResponse>(data: parsedResponse);
      }
      return BaseApiResponse<FiveDaysWeatherResponse>(errorCode: data.statusCode, errorMessage: 'Error on fetching data');
    }).catchError((e) {
      print('caught an error $e');
      return BaseApiResponse<FiveDaysWeatherResponse>(errorMessage: 'Error on fetching data');
    });
  }
}
