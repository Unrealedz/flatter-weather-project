import 'package:flutter/material.dart';
import 'package:flutter_app/ui/weather_card.dart';
import 'package:flutter_app/api/WeatherService.dart';
import 'package:flutter_app/api/model/base_response.dart';
import 'package:flutter_app/model/hour_weather_dao.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  BaseApiResponse<FiveDaysWeatherResponse> data;
  Future future;
  List<WeatherData> list = [];

  @override void initState(){
    super.initState();
    future = _loadData();
  }

  Future<BaseApiResponse<FiveDaysWeatherResponse>> _loadData() async {
    var response = await WeatherService().getFiveDaysWeather();
    return response;
  }

  Widget buildItem(WeatherData dayWeather) {
    return WeatherCard(dayWeather);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.blueAccent,
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    future = _loadData();
                  });
                  },
                child: Icon(
                  Icons.refresh,
                  size: 26.0,
                ),
              ),
            )
          ]
      ),
      body: Container(
        child: FutureBuilder<BaseApiResponse<FiveDaysWeatherResponse>>(
            future: future,
            builder: (BuildContext context, AsyncSnapshot<BaseApiResponse<FiveDaysWeatherResponse>> snapshot) {
              if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done) {
                return ListView.builder(itemBuilder: (_, index) => buildItem(snapshot.data.data.listDao[index]),
                    itemCount: snapshot.data.data.listDao.length);
              }
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else if(snapshot.data.errorMessage != null){
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text(snapshot.data.errorMessage),
                    duration: Duration(seconds: 3),
                  ));
                });
                return Center(child: Text('No data available'),);
              }
              return Container();
            }
        ),
      ),
    );
  }
}

