import 'package:flutter/material.dart';
import 'package:flutter_app/model/hour_weather_dao.dart';

import '../styles.dart';
import '../utils.dart';

class WeatherDetailPage extends StatelessWidget {

  final WeatherData data;

  WeatherDetailPage({Key key, @required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text("Weather detail"),
        backgroundColor: Colors.blueAccent,
      ),
      body:
          Container(
            alignment: Alignment.center,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(Utils.formatShortDateTime(data.dt),
                    style: TextStyle(
                      fontSize: 36,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey
                  ),),
                  SizedBox(height: 36,),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text(Utils.formatTemperature(data.main.temp),
                      style: TextStyle(
                        fontSize: 48,
                        fontWeight: FontWeight.bold,
                        color: Colors.blue
                    ),),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text(data.weathersFirst() != null ? data.weathersFirst().description : '', style: TextStyle(
                        fontSize: 24,
                        color: Colors.lightBlueAccent)
                      ,),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text('Pressure: ${data.main.pressure}',
                      style: simpleTextStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text('Humidity: ${data.main.humidity}' ,
                        style: simpleTextStyle
                      ),
                  )
                ]
            ),
          )
    );
  }
}
